﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Conditions:
// A = No movement, viewpoint 1.
// B = movement to other than 1 or 2, viewpoint 1
// C = movement to vp2, viewpoint 2
// D = movement to other than 1 or 2, viewpoint 2

public class GameController : MonoBehaviour {

    public PlayerTarget[] playerTargets;
    public int trialsPerCondition;
    public ObjectManager objectManager;
    public PlayerController playerController;
    public float showTime = 10f;
    public float minMoveTime = 7f;
    public InputField pidInput;

    //UI
    public CanvasGroup startGroup;
    public GameObject readyButton;
    public CanvasGroup answerGroup;
    public Text roundText;
    public Text statusText;
    public Text timerText;

    private enum Condition { A, B, C, D }
    private List<Condition> trials = new List<Condition>();

    private string pid;
    private bool isReady;
    private bool hasAnswered;
    private bool wasCorrect;

    private float t;

    private ExpLog logger;

    void Start()
    {
        // Setup random trials list
        List<Condition> temp = new List<Condition>();
        for (int i = 0; i < trialsPerCondition; i++) {
            foreach (Condition c in System.Enum.GetValues(typeof(Condition))) {
                temp.Add(c);
            }
        }
        while (temp.Count > 0) {
            Condition condition = temp[Random.Range(0, temp.Count)];
            trials.Add(condition);
            temp.Remove(condition);
        }

        // UI stuff
        statusText.text = "Fill ID and press START";
        ToggleCanvasGroup(startGroup, true);
        readyButton.SetActive(false);
        ToggleCanvasGroup(answerGroup, false);
    }

    public void Run()
    {
        ToggleCanvasGroup(startGroup, false);
        // Get participant ID
        pid = pidInput.text;
        if (pid == "") {
            pid = "_unk";
        }

        // Create data file
        if (logger != null) {
            logger.Close();
        }
        string[] header = new string[] { "pid", "trial", "condition", "object", "correct", "location", "viewpoint" };
        logger = ExpLog.Create("log", ExpLog.MakeFileName(string.Format("{0}", pid)), header);

        // Start trials
        StartCoroutine(RunTrials());
    }

    private IEnumerator RunTrials()
    {
        for (int i = 0; i < trials.Count; i++) {
            yield return RunTrial(i);
        }
        // Close logger
        logger.Close();
    }

    private IEnumerator RunTrial(int numTrial)
    {
        roundText.text = string.Format("{0}-{1}", numTrial + 1, trials[numTrial]);
        Debug.Log(string.Format("Starting Trial {0}, Condition {1}", numTrial + 1, trials[numTrial]));

        // RESET
        isReady = false;
        hasAnswered = false;
        playerController.BlindPlayer();
        SelectPlayerTarget(playerTargets[0]);
        playerController.Reset();
        readyButton.SetActive(true);
        statusText.text = string.Format("Move to {0} \n Press Ready when ready", playerTargets[0].name);
        Debug.Log(statusText.text);

        Condition condition = trials[numTrial];

        // MEANWHILE
        // Pick target position and viewpoint based on condition. Default is A.
        PlayerTarget target = playerTargets[0];
        if (condition == Condition.B || condition == Condition.D) {
            target = playerTargets[Random.Range(2, playerTargets.Length)];
        } else if (condition == Condition.C) {
            target = playerTargets[1];
        }

        PlayerTarget viewpoint = playerTargets[0];
        if (condition == Condition.C || condition == Condition.D) {
            viewpoint = playerTargets[1];
        }

        // Set objects
        objectManager.Initialize();


        // Wait for player ready
        yield return new WaitUntil(() => {
            return isReady;
        });
        readyButton.SetActive(false);

        // Eyes open for showTime seconds
        t = showTime;
        statusText.text = string.Format("Showing for {0} seconds", showTime);
        playerController.UnblindPlayer();
        yield return new WaitForSeconds(showTime);
        playerController.BlindPlayer();

        // Instruct experimenter
        SelectPlayerTarget(target);
        readyButton.SetActive(true);
        statusText.text = string.Format("Move to {0} \n Press Ready when ready", target.name);
        Debug.Log(statusText.text);

        // Move object
        yield return new WaitForEndOfFrame();
        Transform movedObject = objectManager.MoveRandomObject();

        // Wait for completed movement, minimum of minMoveTime
        t = minMoveTime;
        isReady = false;

        yield return new WaitUntil(() => {
            return (isReady && t <= 0);
        });

        readyButton.SetActive(false);
        ToggleCanvasGroup(answerGroup, true);

        statusText.text = string.Format("Select Answer", viewpoint.name);

        // Move player to target viewpoint
        playerController.SetPosition(viewpoint.transform.position);

        // Eyes open
        playerController.UnblindPlayer();

        // Wait answer
        yield return new WaitUntil(() => {
            return hasAnswered;
        });

        ToggleCanvasGroup(answerGroup, false);

        // Store data
        string[] datarow = new string[] { pid, (numTrial + 1).ToString(), condition.ToString(), movedObject.name, (wasCorrect) ? "1" : "0", target.name, viewpoint.name};
        logger.LogAndFlush(datarow);

        Debug.Log(string.Format("Trial {0} completed. Condition {1}. Answer was {2}. Data was saved.", numTrial + 1, condition, (wasCorrect) ? "correct" : "incorrect"));
        statusText.text = "End of experiment. \nData saved.";
    }

    public void SetReady()
    {
        isReady = true;
    }

    public void Answer(bool correct)
    {
        hasAnswered = true;
        wasCorrect = correct;
    }

    private void SelectPlayerTarget(PlayerTarget target)
    {
        foreach (PlayerTarget playerTarget in playerTargets) {
            if (playerTarget == target) {
                playerTarget.Select();
            } else {
                playerTarget.Deselect();
            }
        }
    }

    void Update()
    {
        if (t > 0)
        {
            t -= Time.deltaTime;
            timerText.text = ((int)t).ToString();
        }
    }

    private void ToggleCanvasGroup(CanvasGroup canvasGroup, bool state)
    {
        canvasGroup.alpha = (state) ? 1f: 0;
        canvasGroup.interactable = state;
    }
}
