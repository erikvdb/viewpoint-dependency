﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class PlayerTarget : MonoBehaviour {

    public Color selectedColor = Color.cyan;
    public Color deselectedColor = Color.red;
    public TextMesh labelMesh;

    private Renderer rend;

	void Start ()
    {
        rend = GetComponent<Renderer>();

        // Set label
        labelMesh.text = gameObject.name;
        labelMesh.transform.rotation = Quaternion.Euler(90f, -23f, 0); // Counteract camera rotation. Should be dynamic but is hard-coded for now.
	}

    public void Select()
    {
        rend.material.SetColor("_Color", selectedColor);
    }

    public void Deselect()
    {
        rend.material.SetColor("_Color", deselectedColor);
    }
}
