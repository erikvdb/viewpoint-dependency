﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour {

    public Transform[] positions;
    public Transform[] objects;
    public Color[] colors;
    public Text feedback;
    public Image feedbackColorBox;

    private List<Transform> availablePositions;
    private List<Color> availableColors;

	// Use this for initialization
	void Start () {

	}

	public void Initialize()
    {
        // Populate available positions
        availablePositions = new List<Transform>();
        foreach (Transform position in positions) {
            availablePositions.Add(position);
        }

        // Populate available colors
        availableColors = new List<Color>();
        foreach (Color color in colors) {
            availableColors.Add(color);
        }

        // Place and color objects
        foreach (Transform obj in objects) {
            obj.position = GetAvailablePosition();
            obj.rotation = Quaternion.Euler(new Vector3(obj.eulerAngles.x, Random.Range(0, 360), obj.eulerAngles.z));

            Renderer rend = obj.GetComponent<Renderer>();
            if (rend != null) {
                Color color = GetAvailableColor();
                rend.material.SetColor("_Color", color);
            }
        }

        feedback.text = "Objects initialized";
    }

    public Transform MoveRandomObject()
    {
        Transform obj = objects[Random.Range(0, objects.Length)];
        obj.position = GetAvailablePosition();

        feedback.text = obj.name;
        feedbackColorBox.color = obj.GetComponent<Renderer>().material.GetColor(Shader.PropertyToID("_Color"));
        Debug.Log(feedback.text + "was moved!");
        return obj;
    }

    private Vector3 GetAvailablePosition()
    {
        Transform pick = availablePositions[Random.Range(0, availablePositions.Count)];
        availablePositions.Remove(pick);
        return pick.position;
    }

    private Color GetAvailableColor()
    {
        Color pick = availableColors[Random.Range(0, availableColors.Count)];
        availableColors.Remove(pick);
        return pick;
    }
}
