﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Transform transporter;
    public Transform player;
    public GameObject blinder;

    public Renderer[] playerMeshes;
    public Color blindColor;
    public Color unblindColor;

    public Vector3 offset; // Use to offset Vive depending on your play area

    void Start()
    {
        UnblindPlayer();
        Reset();
    }

	public void SetPosition(Vector3 position)
    {
        float dx = position.x - player.position.x;
        float dz = position.z - player.position.z;
        transporter.position = offset + new Vector3(dx, 0, dz);
    }

    public void Reset()
    {
        transporter.position = offset;
    }

    public void BlindPlayer()
    {
        blinder.SetActive(true);
        SetPlayerColor(blindColor);
        Debug.Log("Player blinded");
    }

    public void UnblindPlayer()
    {
        blinder.SetActive(false);
        SetPlayerColor(unblindColor);
        Debug.Log("Player unblinded");
    }

    private void SetPlayerColor(Color color)
    {
        foreach (Renderer rend in playerMeshes) {
            rend.material.SetColor("_Color", color);
        }
    }
}
