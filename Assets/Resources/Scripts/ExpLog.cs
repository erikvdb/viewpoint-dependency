﻿/*
Copyright (c) 2017 Arvind Datadien, Radboud University Nijmegen
Used and distributed with permission.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class ExpLog
{

    // Directory where files are stored. If non-existant, will be created when Create is called.
    // Application.dataPath is Assets folder in Unity editor or project_Data folder in build.
    public static readonly string DIR = Application.dataPath + "/../ExperimentData";

    // Static instances of stores
    private static Dictionary<string, ExpLog> stores = new Dictionary<string, ExpLog>();


    private string logName; // Name of log, used to access it and write to it
    private string fileName; // Filename on disk, without path, with extension
    private string[] fields; // Column/field names
    private StreamWriter writer; // Writer used to write to file


    // Static instance methods

    public static ExpLog Create(string logName, string fileName, params string[] fields)
    {
        ExpLog store = new ExpLog(logName, fileName, fields);
        // BODGE
        if (Exists(logName))
        {
            stores[logName] = store;
        }
        else
        {
            stores.Add(logName, store);
        }
        return Get(logName);
    }

    public static ExpLog Get(string logName)
    {
        return stores[logName];
    }

    public static Boolean Exists(string logName)
    {
        return stores.ContainsKey(logName);
    }


    // Static utility methods

    public static string CleanFileName(string rawFileName)
    {
        Regex rgx = new Regex("[^a-zA-Z0-9 /.-]"); // Todo: allow dot .
        return rgx.Replace(rawFileName, "");
    }

    public static string MakeFileName(string baseName = "data")
    {
        string fileName = baseName;
        fileName += "-";
        fileName += DateTime.Now.ToString("yyyyMMddHHmm");
        fileName += ".tsv";
        return CleanFileName(fileName);
    }


    // Object methods

    // Private constructor, should use static Create() method
    private ExpLog(string logName, string fileName, string[] fields)
    {
        fileName = CleanFileName(fileName); // Cleaning may result in empty string
        if ("".Equals(fileName))
        {
            fileName = MakeFileName(logName);
        }

        this.logName = logName;
        this.fileName = fileName;
        this.fields = fields;


        // Create directory if needed (does nothing if it exists)
        Directory.CreateDirectory(DIR);

        // Create StreamWriter to write to file. Do not append; overwrite any existing file.
        string filePath = DIR + "/" + this.fileName;
        this.writer = new StreamWriter(filePath, false);

        // First line will be the header with field names
        LogAndFlush(fields);
    }

    // Logs data. Data is buffered, call Flush() to write to file. Returns itself so you can use myLog.Log().Flush()
    public ExpLog Log(params string[] values)
    {
        if (values.Length != fields.Length)
        {
            throw new ArgumentException(
                String.Format(
                    "Logging {0} values while {1} fields were defined for store '{2}'.",
                    values.Length, fields.Length, logName
                )
            );
        }

        string line = string.Join("\t", values); // tab separated values
        writer.WriteLine(line);

        return this;
    }

    // Convenience method to log and write to file.
    public ExpLog LogAndFlush(params string[] values)
    {
        Log(values);
        Flush();

        return this;
    }

    // Writes any buffered data to the file on disk (may cause slowdown if called too often).
    public ExpLog Flush()
    {
        writer.Flush();

        return this;
    }

    public void Close()
    {
        writer.Close();
    }
}
