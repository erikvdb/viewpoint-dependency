// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VD/AmbientOcclusion"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[Toggle(_USETEXTURE_ON)] _UseTexture("UseTexture", Float) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Radius("Radius", Range( 0 , 1)) = 1
		_Intensity("Intensity", Range( 0 , 1)) = 0.1900564
		_InnerRadius("InnerRadius", Float) = 0
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull Off
		

		Pass
		{
			CGPROGRAM
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#pragma shader_feature _USETEXTURE_ON


			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				
			};

			uniform sampler2D _MainTex;
			uniform fixed4 _Color;
			uniform float _Radius;
			uniform float _InnerRadius;
			uniform float _Intensity;
			uniform sampler2D _TextureSample0;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.texcoord.xy = v.texcoord.xy;
				o.texcoord.zw = v.texcoord1.xy;
				
				// ase common template code
				
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				fixed4 myColorVar;
				// ase common template code
				float2 uv14 = i.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_18_0 = length( (float2( -1,-1 ) + (uv14 - float2( 0,0 )) * (float2( 1,1 ) - float2( -1,-1 )) / (float2( 1,1 ) - float2( 0,0 ))) );
				float smoothstepResult29 = smoothstep( 0 , _Radius , temp_output_18_0);
				float temp_output_3_0_g1 = ( _InnerRadius - temp_output_18_0 );
				float4 temp_cast_0 = (( smoothstepResult29 - ( saturate( ( temp_output_3_0_g1 / fwidth( temp_output_3_0_g1 ) ) ) * _Intensity ) )).xxxx;
				float2 uv46 = i.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				#ifdef _USETEXTURE_ON
				float4 staticSwitch50 = tex2D( _TextureSample0, uv46 );
				#else
				float4 staticSwitch50 = temp_cast_0;
				#endif
				
				
				myColorVar = ( staticSwitch50 * _Color );
				return myColorVar;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15201
1931;29;1902;1014;237.9247;307.6724;1.4;True;True
Node;AmplifyShaderEditor.CommentaryNode;39;-2208.125,-46.52034;Float;False;2268.621;589.0999;Circle;10;33;29;37;27;21;34;18;17;14;57;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;14;-2158.125,124.6401;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;17;-1840.925,127.2398;Float;False;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;1,1;False;3;FLOAT2;-1,-1;False;4;FLOAT2;1,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-1523.127,408.8515;Float;False;Property;_InnerRadius;InnerRadius;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LengthOpNode;18;-1609.006,141.7997;Float;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;-1610.087,32.71911;Float;False;Property;_Radius;Radius;2;0;Create;True;0;0;False;0;1;0.1946031;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;34;-1143.504,221.3396;Float;True;Step Antialiasing;-1;;1;2a825e80dfb3290468194f83380797bd;0;2;1;FLOAT;0;False;2;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-1209.785,441.0194;Float;False;Property;_Intensity;Intensity;3;0;Create;True;0;0;False;0;0.1900564;0.01;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;46;-639.7072,-298.0598;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;29;-1171.224,3.479659;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-836.7437,224.4604;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0.24;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;33;-541.8636,200.8401;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;42;-337.4645,-325.8201;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;50;210.0131,39.94069;Float;True;Property;_UseTexture;UseTexture;0;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;7;121.9391,510.5206;Float;False;0;0;_Color;Shader;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;523.9409,412.4204;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;68;1024.383,290.1604;Float;False;True;2;Float;ASEMaterialInspector;0;2;VD/AmbientOcclusion;6e114a916ca3e4b4bb51972669d463bf;0;0;SubShader 0 Pass 0;2;False;False;True;Off;False;False;False;False;False;True;1;RenderType=Opaque;False;0;0;0;False;False;False;False;False;False;False;False;False;True;2;0;0;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;17;0;14;0
WireConnection;18;0;17;0
WireConnection;34;1;18;0
WireConnection;34;2;57;0
WireConnection;29;0;18;0
WireConnection;29;2;27;0
WireConnection;37;0;34;0
WireConnection;37;1;21;0
WireConnection;33;0;29;0
WireConnection;33;1;37;0
WireConnection;42;1;46;0
WireConnection;50;1;33;0
WireConnection;50;0;42;0
WireConnection;9;0;50;0
WireConnection;9;1;7;0
WireConnection;68;0;9;0
ASEEND*/
//CHKSM=0D9DEAD67FE5134E73493BCD67CB82FCAF8AC4C3